#!/usr/bin/env python

import argparse
import requests
import base64
import http.server
import time
import json

DEFAULT_HOST_NAME = "0.0.0.0"
DEFAULT_PORT_NUMBER = 9000


def create_handler(token):
    class Handler(http.server.BaseHTTPRequestHandler):
        def do_HEAD(s):
            s.send_response(200)
            s.send_header("Content-type", "application/json")
            s.end_headers()

        def do_GET(s):
            """Respond to a GET request."""
            s.send_response(200)
            s.send_header("Content-type", "application/json")
            s.end_headers()

            data = get_from_spotify("https://api.spotify.com" + s.path, token)

            # Fixup image URLs for incompatible clients
            if "images" in data:
                for image in data["images"]:
                    image["url"] = image["url"].replace("https", "http", 1)

            s.wfile.write(json.dumps(data).encode("utf-8"))

    return Handler


def get_access_token(api_key):
    encodedString = base64.b64encode(api_key.encode("utf-8"))
    r = requests.post(
        "https://accounts.spotify.com/api/token",
        data={"grant_type": "client_credentials"},
        headers={"Authorization": "Basic %s" % encodedString.decode("utf-8")},
    )
    loaded = r.json()
    return loaded["access_token"]


def get_from_spotify(url, access_token):
    r = requests.get(
        url, params=None, headers={"Authorization": "Bearer %s" % access_token}
    )
    return r.json()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--address", dest="address", default=DEFAULT_HOST_NAME)
    parser.add_argument("-p", "--port", dest="port", default=DEFAULT_PORT_NUMBER)
    parser.add_argument("-k", "--api-key", dest="api_key", required=True)
    args = parser.parse_args()

    access_token = get_access_token(args.api_key)

    server_class = http.server.HTTPServer
    httpd = server_class((args.address, args.port), create_handler(access_token))

    print("Listening on {}:{}".format(args.address, args.port))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
