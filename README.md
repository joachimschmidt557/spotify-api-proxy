# spotify-api-proxy

A simple proxy for the [Spotify
API](https://developer.spotify.com/documentation/web-api/) for legacy
apps which do not support the autorization method used by the API.
